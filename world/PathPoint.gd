tool
extends Position3D

export(bool) var reset_inouts = false setget _set_reset_inouts
export(float) var tilt = 0 setget _set_tilt

var index = -1

signal update_point
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func node_updated():
	if index != -1:
		print("node", index, "updated")
		emit_signal("update_point",index,self)

func _notification(what):
	if what == NOTIFICATION_TRANSFORM_CHANGED:
		node_updated()

func _set_reset_inouts(value):
	if value and is_inside_tree():
		$in.transform = Transform.IDENTITY
		$out.transform = Transform.IDENTITY
		node_updated()

func _set_tilt(value):
	tilt = value
	if is_inside_tree():
		node_updated()
