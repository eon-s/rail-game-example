tool
extends Path

export(bool) var reset = false setget set_reset
export(bool) var closed_path = false

func _ready():
	pass

func set_reset(value):
	if value:
		if curve == null:
			curve = Curve3D.new()
		curve.clear_points()
		recreate_curve()

func recreate_curve():
	var child_points = $curve_points.get_children()
	
	var index = 0
	for pos in child_points:
		if not pos.is_connected("update_point", self, "point_changed"):
			pos.connect("update_point", self, "point_changed")
		curve.add_point(pos.translation,pos.get_node("in").translation,pos.get_node("out").translation)
		curve.set_point_tilt(index,pos.tilt)
		pos.index = index
		index+=1
	if closed_path:
		curve.add_point(child_points[0].translation,child_points[0].get_node("in").translation,child_points[0].get_node("out").translation)
	

func point_changed(index,point):
	curve.set_point_position(index,point.translation)
	curve.set_point_in(index,point.get_node("in").translation)
	curve.set_point_out(index,point.get_node("out").translation)
	curve.set_point_tilt(index,point.tilt)
