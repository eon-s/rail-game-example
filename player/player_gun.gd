extends Spatial

export(PackedScene) var normal_shot
export(PackedScene) var charged_shot

var charge_time

func _ready():
	print(2 in range(2,10))
	charge_time = 0.0
	set_process(false)

func start_charge():
	set_process(true)

func stop_charge():
	charge_time = 0.0
	set_process(false)


func _process(delta):
	charge_time += delta

func shot():
	charge_time = 0.0
	var shot_instance = normal_shot.instance()
	shot_instance.speed = 200
	
	get_tree().current_scene.add_child(shot_instance)
	(shot_instance as Spatial).global_transform =$Position3D.global_transform
	
	shot_instance.direction = transform.basis.xform(Vector3.FORWARD)
	
	shot_instance = normal_shot.instance()
	shot_instance.speed = 200
	
	get_tree().current_scene.add_child(shot_instance)
	(shot_instance as Spatial).global_transform = $Position3D2.global_transform
	
	shot_instance.direction = transform.basis.xform(Vector3.FORWARD)
	
