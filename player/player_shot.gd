extends Area

var speed = 1
var direction = Vector3.FORWARD
var lifetime = 10

func _ready():
	get_tree().create_timer(lifetime).connect("timeout",self,"queue_free")


func _physics_process(delta):
	translate(direction*speed*delta)
