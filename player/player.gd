extends KinematicBody

export (float) var motion_speed = 10
export(NodePath) var look_at_path
var look_at_node

func _ready():
	look_at_node = get_node(look_at_path)

func _physics_process(delta):
	var direction = Vector3.ZERO
	direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	direction.y = Input.get_action_strength("move_up") - Input.get_action_strength("move_down")
	
	translate(direction.normalized()*motion_speed*delta)
	
	look_at_node.translate(direction.normalized()*motion_speed*delta*-0.5)
	
	translation.x = clamp(translation.x,-9,9)
	translation.y = clamp(translation.y,0,10)
	
	look_at_node.translation.x = clamp(translation.x,-2,2)
	look_at_node.translation.y = clamp(translation.y,3,5)
	
	#$hull.look_at(global_transform.origin + get_parent().transform.basis.xform(Vector3.FORWARD)*0.01,Vector3.UP)
	$hull.look_at(look_at_node.global_transform.origin ,Vector3.UP)
	
	if Input.is_action_just_released("shoot"):
		$guns.shot()
		
